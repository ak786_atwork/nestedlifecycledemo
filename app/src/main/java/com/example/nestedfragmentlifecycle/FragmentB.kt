package com.example.nestedfragmentlifecycle


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * A simple [Fragment] subclass.
 */
class FragmentB : Fragment {

    lateinit var fragmentInteraction: FragmentInteraction

    constructor(fragmentInteraction: FragmentInteraction) : super() {
        this.fragmentInteraction = fragmentInteraction
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        showLog("oncreateview")

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_b, container, false)

        view.findViewById<TextView>(R.id.openFragmentA).setOnClickListener {
            fragmentInteraction.openFragmentA()
        }
        view.findViewById<TextView>(R.id.openChildFragmentB).setOnClickListener {
            openChildFragment()
        }

        // Inflate the layout for this fragment
        return view
    }

    private fun openChildFragment() {

    }

    //lifecycle callbacks
    override fun onCreate(savedInstanceState: Bundle?) {
        showLog("oncreate")
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        showLog("onattach called")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showLog("onactivitycreated")
    }

    override fun onStart() {
        super.onStart()
        showLog("on start")
    }

    override fun onResume() {
        super.onResume()
        showLog("onresume")
    }

    override fun onPause() {
        super.onPause()
        showLog("onpause")
    }

    override fun onStop() {
        super.onStop()
        showLog("onstop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showLog("ondestroyview")
    }

    override fun onDestroy() {
        super.onDestroy()

        showLog("ondestroy")
    }


    override fun onDetach() {
        super.onDetach()

        showLog("ondetach")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        showLog("onsaveinstance")
    }

    private fun showLog(info: String) {
        Log.d("LIFECYCLE-FRAGMENT-B", info)
    }


}
