package com.example.nestedfragmentlifecycle

interface FragmentInteraction {
    fun openFragmentB()
    fun openFragmentA()
}