package com.example.nestedfragmentlifecycle

import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FragmentInteraction {

    lateinit var fragmentA: FragmentA

    override fun openFragmentB() {
        val fragmentB = FragmentB(this)
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        fragmentTransaction.add(R.id.container, fragmentB)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun openFragmentA() {
         fragmentA = FragmentA(this)
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.container, fragmentA)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        showLog("oncreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        openFragmentA.setOnClickListener {
            container.visibility = View.VISIBLE
            openFragmentA()
        }

        openFragmentB.setOnClickListener {
            container.visibility = View.VISIBLE
            openFragmentB()
        }

    }

    //lifecycle callback
    override fun onStart() {
        super.onStart()
        showLog("onstart")
    }

    override fun onRestart() {
        super.onRestart()
        showLog("on restart")
    }

    override fun onResume() {
        super.onResume()
        showLog("onresume")
    }

    override fun onPause() {
        super.onPause()
        showLog("on pause")
    }

    override fun onStop() {
        showLog("onstop called")
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        showLog("onsaveinstance called")
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        showLog("onrestore instance")
    }

    override fun onDestroy() {
        super.onDestroy()
        showLog("ondestroy")
    }

    private fun showLog(info: String) {
        Log.d("LIFE-ACTIVITY- ", info)
    }


}
